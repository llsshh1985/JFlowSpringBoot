package BP.WF.Port.Admin2;

import BP.DA.*;
import BP.En.*;
import BP.Web.*;
import BP.WF.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 独立组织属性
*/
public class OrgAttr extends EntityNoNameAttr
{
	/** 
	 管理员帐号
	*/
	public static final String Adminer = "Adminer";
	/** 
	 管理员名称
	*/
	public static final String AdminerName = "AdminerName";
	/** 
	 父级组织编号
	*/
	public static final String ParentNo = "ParentNo";
	/** 
	 父级组织名称
	*/
	public static final String ParentName = "ParentName";
}