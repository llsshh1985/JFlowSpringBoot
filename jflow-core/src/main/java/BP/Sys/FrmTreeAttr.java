package BP.Sys;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.Sys.*;
import java.util.*;

/** 
 属性
*/
public class FrmTreeAttr extends EntityTreeAttr
{
	/** 
	 数据源
	*/
	public static final String DBSrc = "DBSrc";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
}