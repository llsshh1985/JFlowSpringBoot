package BP.Port;

import BP.DA.*;
import BP.En.*;
import BP.Sys.*;
import java.util.*;
import java.io.*;

/** 
 操作员属性
*/
public class EmpAttr extends BP.En.EntityNoNameAttr
{

		///#region 基本属性
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 密码
	*/
	public static final String Pass = "Pass";
	/** 
	 sid
	*/
	public static final String SID = "SID";

		///#endregion
}