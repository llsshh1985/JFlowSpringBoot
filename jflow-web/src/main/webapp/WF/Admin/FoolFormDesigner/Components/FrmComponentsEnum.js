﻿
//表单组件.
if (typeof FrmComponents == "undefined") {
    var FrmComponents = {}
   
        FrmComponents.Map = 4,//地图控件
        FrmComponents.MicHot = 5,//录音控件
        FrmComponents.AthShow = 6,//字段附件
        FrmComponents.MobilePhoto = 7,
        FrmComponents.HandWriting = 8,//签字版 
        FrmComponents.HyperLink = 9,//超链接
        FrmComponents.Lab = 10,//文本
        FrmComponents.FrmImg = 11,//图片
        FrmComponents.FrmImgAth = 12,//图片附件
        FrmComponents.IDCard =13,//身份证
        FrmComponents.SignCheck = 14,//签批组件
        FrmComponents.FlowBBS = 15,//评论组件
        FrmComponents.Fiexed = 16,//系统定位
        FrmComponents.DocWord = 17,// 公文字号
        FrmComponents.JobSchedule = 50,//流程进度图
        FrmComponents.BigText = 60,//大文本
        FrmComponents.Ath = 70,//独立附件
        FrmComponents.Dtl = 80,//从表
        FrmComponents.Frame = 90,//框架
        FrmComponents.Score = 101//评分控件
}
 